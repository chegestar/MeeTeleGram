SOURCES += $$PWD/main.cpp \
    $$PWD/setting.cpp \
    $$PWD/qtconnection.cpp \
    $$PWD/qtelegram.cpp \
    $$PWD/qttimer.cpp \
    $$PWD/qtupdate.cpp \
    $$PWD/qpeerid.cpp

HEADERS += \
    $$PWD/setting.h \
    $$PWD/qtelegram.h \
    $$PWD/qpeerid.h
